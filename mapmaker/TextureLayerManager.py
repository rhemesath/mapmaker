from pathlib import Path
from LayerManager import LayerManager
from Dirtiness import Dirtiness

class TextureLayerManager(LayerManager):
    def __init__(self, map_maker, tile_size: int, file_path:Path=None):
        super().__init__(map_maker)

        self.tile_size = tile_size
        self._file_path = file_path
        self._selected_color = (255, 255, 255, 130)
        self._selected_tile_x = 0
        self._selected_tile_y = 0

    def get_file_path(self) -> Path:
        return self._file_path
    def set_file_path(self, file_path: Path):
        self._file_path = file_path
        if self._layer is not None: self._layer.resetup()
        Dirtiness.change()

    def reset(self):
        super().reset()
        self._selected_tile_x = 0
        self._selected_tile_y = 0
        self.remove_layer()

    def set_layer(self, layer):
        if self._layer is not None and layer is not self._layer:
            self._layer.close()
        super().set_layer(layer)
        Dirtiness.change()

    def remove_layer(self):
        if self._layer is None: return
        self._layer.close()
        self._layer = None
        Dirtiness.change()

    def mouse_clicked(self, mouse_x: int, mouse_y: int):
        if self._layer is None: return
        self.set_selected_tile(
            self._layer.texel_x_to_tile_x(self.real_x_to_texel_x(mouse_x)),
            self._layer.texel_y_to_tile_y(self.real_y_to_texel_y(mouse_y))
        )

    def _get_selected_tile_rect(self) -> tuple:
        if self.get_selected_tile() == -1: return None
        ts = self._layer.ts
        return (self._selected_tile_x * ts, self._selected_tile_y * ts, ts, ts)

    def _get_selected_tile(self, tile_x: int, tile_y: int) -> int:
        if self._layer is None: return -1
        return self._layer.get_tile(tile_x, tile_y)

    def set_selected_tile(self, tile_x: int, tile_y: int):
        if self._get_selected_tile(tile_x, tile_y) == -1: return
        self._selected_tile_x = tile_x
        self._selected_tile_y = tile_y

    def get_selected_tile(self) -> int:
        return self._get_selected_tile(self._selected_tile_x, self._selected_tile_y)

    def get_selected_tile_pos(self) -> tuple:
        return (self._selected_tile_x, self._selected_tile_y)

    def render(self):
        super().render()
        ren_x, ren_y, _, _ = self._get_render_rect()
        self._render_hover_rect(self._selected_color, self._get_selected_tile_rect(), ren_x, ren_y)
