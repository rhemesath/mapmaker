from events import DIRTINESS_CHANGED, NEW_MAP

class Dirtiness:
    _dirtiness: int = -1
    _map_maker = None

    @classmethod
    def setup(cls, map_maker):
        cls._map_maker = map_maker

        cls._map_maker.event.listen(NEW_MAP, lambda data: cls.reset())

    @classmethod
    def get(cls) -> int:
        return cls._dirtiness
    @classmethod
    def reset(cls):
        cls._set(0)
    @classmethod
    def change(cls):
        cls._set(cls._dirtiness + 1)

    @classmethod
    def _set(cls, dirtiness: int):
        if cls._dirtiness == dirtiness: return
        cls._dirtiness = dirtiness
        cls._map_maker.event.emit(DIRTINESS_CHANGED, dirtiness)
