
class Command:
    def do(self): pass
    def undo(self): pass


class DrawTileCommand(Command):
    def __init__(self, draw_layer, tile_i: int, tile_x: int, tile_y: int):
        self._layer = draw_layer
        self._tile_i = tile_i
        self._prev_tile_i = -1
        self._tile_x = tile_x
        self._tile_y = tile_y

    def do(self):
        self._prev_tile_i = self._layer.get_tile(self._tile_x, self._tile_y)
        if self._prev_tile_i == -1: return
        self._layer.set_tile(self._tile_i, self._tile_x, self._tile_y)

    def undo(self):
        if self._prev_tile_i == -1: return
        self._layer.set_tile(self._prev_tile_i, self._tile_x, self._tile_y)


class DrawTilesCommand(Command):
    def __init__(self, draw_layer, tile_i: int):
        self.layer = draw_layer
        self._tile_i = tile_i
        self._tiles = []
        self._cmds = []
        self.is_empty = True

    def add_line(self, x1: int, y1: int):
        if len(self._cmds) == 0:
            self._add(x1, y1)
            return

        # Bresenham's line drawing algorithm
        x0, y0 = self._tiles[-1]

        dx =  abs(x1 - x0)
        dy = -abs(y1 - y0)
        sx = 1 if x0 < x1 else -1
        sy = 1 if y0 < y1 else -1
        err = dx + dy
        while True:
            self._add(x0, y0)
            if x0 == x1 and y0 == y1: break
            e2 = err * 2
            if e2 >= dy:
                err += dy
                x0 += sx
            if e2 <= dx:
                err += dx
                y0 += sy

    def _add(self, tile_x: int, tile_y: int):
        if (tile_x, tile_y) in self._tiles: return
        self.is_empty = False
        self._tiles.append((tile_x, tile_y))
        cmd = DrawTileCommand(self.layer, self._tile_i, tile_x, tile_y)
        cmd.do()
        self._cmds.append(cmd)

    def do(self):
        for tile_cmd in self._cmds: tile_cmd.do()

    def undo(self):
        for tile_cmd in self._cmds: tile_cmd.undo()
