from sgtk import TextureWidget
from sgtk.Widget import WIDGET_COLOR as DEFAULT_COLOR

from LayerManager import LayerManager
from MapWidgetInput import MapWidgetInput

class MapWidget(TextureWidget):
    def __init__(self, map_maker, w: int, h: int, layer_manager: LayerManager,
        bg_color:tuple=DEFAULT_COLOR, pad_x:int=0, pad_y:int=0, debug_name:str=""
    ):
        self.mm = map_maker
        self.mouse = self.mm.tk.mouse
        super().__init__(self.mm.win, w, h, pad_x=pad_x, pad_y=pad_y, debug_name=debug_name)

        self._bg_color = bg_color
        self._active = False

        self.layer_man = layer_manager

        self.input: MapWidgetInput = None

    def set_input(self, map_widget_input: MapWidgetInput):
        if self.input not in (None, map_widget_input):
            self.input.close()
        self.input = map_widget_input

    def change_layer_manager(self, layer_manager: LayerManager):
        if type(self.layer_man) is LayerManager:
            self.layer_man.close()
        self.layer_man = layer_manager

    def get_rel_mouse_pos(self) -> tuple:
        return (self.mouse.x - self._x, self.mouse.y - self._y)

    def set_active(self, active: bool):
        if active == self._active: return
        self._active = active

    def is_active(self) -> bool:
        return self._active

    def update(self):
        if self.parent is None: return
        # update
        self.input.on_update()

        # render
        self.paint.set_target_texture(self._tex)
        
        self.paint.clear(self._bg_color)
        self.layer_man.render()
        
        self.paint.set_target_texture(None)
        self.request_redraw()

    def close(self, update:bool=True):
        super().close(update=update)
        self.layer_man.close()
        self.input.close()
