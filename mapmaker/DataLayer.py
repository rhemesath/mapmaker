from TextureLayer import TextureLayer

class DataLayer(TextureLayer):
    def _setup_texture(self):
        self.tile_w = 8
        self.tile_h = 1

        self._tex = self.paint.create_target_texture(self.tile_w * self.ts, self.tile_h * self.ts)
        with self.paint.target_texture(self._tex):
            self.paint.clear((0, 0, 0, 0))
            for tile_x in range(1, self.tile_w):
                x = tile_x * self.ts
                color = self._get_color(1.0 / self.tile_w * (tile_x - 1))
                self.paint.rect((*color, 100), x, 0, self.ts, self.ts)
                outline_thickness = 1 #max(1, round(self.ts / 16))
                self.paint.rect_outline((*color, 150), outline_thickness, x, 0, self.ts, self.ts)
        self.set_blended(True)

    def _get_color(self, v: float) -> tuple:
        """ v: [0.0, 1.0]
            v = 0.0 -> red
            v = 1.0 -> red """
        i = int(v * 6)
        f = v * 6 - i
        i %= 6
        if   i == 0: r, g, b = 1  , f  , 0   # [1, 0, 0] -> [1, 1, 0] (red     -> yellow,  g=0 -> g=1)
        elif i == 1: r, g, b = 1-f, 1  , 0   # [1, 1, 0] -> [0, 1, 0] (yellow  -> green,   r=1 -> r=0)
        elif i == 2: r, g, b = 0  , 1  , f   # [0, 1, 0] -> [0, 1, 1] (green   -> cyan,    b=0 -> b=1)
        elif i == 3: r, g, b = 0  , 1-f, 1   # [0, 1, 1] -> [0, 0, 1] (cyan    -> blue,    g=1 -> g=0)
        elif i == 4: r, g, b = f  , 0  , 1   # [0, 0, 1] -> [1, 0, 1] (blue    -> magenta, r=0 -> r=1)
        elif i == 5: r, g, b = 1  , 0  , 1-f # [1, 0, 1] -> [1, 0, 0] (magenta -> red,     b=1 -> b=0)

        return (int(r * 255), int(g * 255), int(b * 255))
