from pathlib import Path
from sdl2 import SDL_SetTextureBlendMode, SDL_BLENDMODE_BLEND, SDL_BLENDMODE_NONE

from Layer import Layer
from events import REFRESH_TEXTURES

class TextureLayer(Layer):
    def __init__(self, layer_manager, name: str):
        super().__init__(layer_manager, layer_manager.tile_size)

        self.name = name
        self._setup()

        self.map_maker.event.listen(REFRESH_TEXTURES, self._on_refresh_textures)

    def set_blended(self, do_blend: bool):
        SDL_SetTextureBlendMode(self._tex,
            SDL_BLENDMODE_BLEND if do_blend else SDL_BLENDMODE_NONE
        )

    def resetup(self):
        super().close()
        self._setup()

    def _setup(self):
        self._setup_texture()
        self._setup_tile_map()
        self.layer_man.tell_size(self.tile_w * self.ts, self.tile_h * self.ts)

    def _setup_texture(self):
        self._tex = self.paint.load_image(self.layer_man.get_file_path())
        w, h = self.paint.get_texture_size(self._tex)
        self.tile_w = w // self.ts
        self.tile_h = h // self.ts

    def _setup_tile_map(self):
        self.tile_map.clear()
        i = 0
        for _ in range(self.tile_h):
            line = []
            for _ in range(self.tile_w):
                line.append(i)
                i += 1
            self.tile_map.append(line)

    def _on_refresh_textures(self, _):
        self.resetup()

    def close(self):
        super().close()
        self.map_maker.event.unlisten(REFRESH_TEXTURES, self._on_refresh_textures)
