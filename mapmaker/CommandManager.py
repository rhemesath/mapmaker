from sgtk.EventManager import EventManager
from commands import Command
from Dirtiness import Dirtiness
from events import CMD_MAN_UPDATE, NEW_MAP

class CommandManager:
    def __init__(self, event_man: EventManager):
        self._event = event_man
        self._cmds = []
        self._i = -1 # index of last used cmd

        self._event.listen(NEW_MAP, lambda _data: self.reset())

    def reset(self):
        self._cmds.clear()
        self._i = -1
        self.emit_update()
        Dirtiness.change()

    def do(self, cmd: Command):
        cmd.do()
        self.add(cmd)

    def undo(self):
        if self._i == -1: return
        self._cmds[self._i].undo()
        self._i -= 1
        self.emit_update()
        Dirtiness.change()

    def redo(self):
        if self._i == len(self._cmds) - 1: return
        self._i += 1
        self._cmds[self._i].do()
        self.emit_update()
        Dirtiness.change()

    def add(self, cmd: Command):
        if self._i != len(self._cmds) - 1:
            self._cmds = self._cmds[: self._i + 1]

        self._cmds.append(cmd)
        self._i = len(self._cmds) - 1
        self.emit_update()
        Dirtiness.change()

    def emit_update(self):
        self._event.emit(CMD_MAN_UPDATE,
            (self._i == -1, self._i == len(self._cmds) - 1)
        )
