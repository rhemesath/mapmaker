from LayerManager import LayerManager
from DrawLayer import DrawLayer
from commands import DrawTilesCommand
from Dirtiness import Dirtiness
from Restorable import Restorable
from events import (
    DRAW_LAYER_ADDED, DRAW_LAYER_REMOVED, DRAW_LAYER_MOVED,
    NEW_MAP, MAP_SIZE_SET, DRAW_LAYER_SET
)

class DrawLayerManager(LayerManager, Restorable):
    def __init__(self, map_maker):
        super().__init__(map_maker)

        self._draw_tiles_cmd: DrawTilesCommand = None
        self._layers = []
        self._layers_named = 0
        self.map_maker.event.listen(NEW_MAP, self._on_new_file_event)
        self.map_maker.event.listen(MAP_SIZE_SET, self._on_map_size_set_event)

    def reset(self):
        super().reset()
        self._draw_tiles_cmd = None
        self._layers_named = 0
        if len(self._layers) > 0:
            for layer in tuple(self._layers): self.remove_layer(layer)
        else:
            self.set_layer(None)

    def set_layer(self, layer: DrawLayer):
        prev_layer = self._layer
        #if prev_layer not in self._layers: prev_layer = None
        self._layer = layer
        self.map_maker.event.emit(DRAW_LAYER_SET, (prev_layer, layer))

    def add_layer(self, layer: DrawLayer):
        if layer in self._layers: return
        self._layers.append(layer)
        self.map_maker.event.emit(DRAW_LAYER_ADDED, layer)
        self.set_layer(layer)
        Dirtiness.change()

    def remove_layer(self, layer: DrawLayer):
        self._layers.remove(layer)
        self.map_maker.event.emit(DRAW_LAYER_REMOVED, layer)
        if self._layer is layer: self.set_layer(None)
        layer.close()
        Dirtiness.change()

    def get_num_layers(self) -> int: return len(self._layers)

    def get_layer_by_name(self, name: str) -> DrawLayer:
        """ Returns: Optional[DrawLayer] """
        for layer in self._layers:
            if layer.name == name: return layer
        return None

    def get_layer_by_row(self, row: int) -> DrawLayer:
        return self._layers[row]

    def get_layer_row(self, layer: DrawLayer) -> int:
        if layer not in self._layers: return None
        return self._layers.index(layer)

    def set_layer_row(self, layer: DrawLayer, row: int):
        if layer not in self._layers: return
        src_row = self.get_layer_row(layer)
        if row < 0 or row >= len(self._layers) or row == src_row: return
        self._layers.remove(layer)
        self._layers.insert(row, layer)
        self.map_maker.event.emit(DRAW_LAYER_MOVED, (layer, src_row))
        Dirtiness.change()

    def close(self):
        for layer in self._layers: layer.close()
        if self._bg_layer is not None: self._bg_layer.close()

    def create_layer_name(self) -> str:
        name = f"Layer {self._layers_named}"
        if name in [layer.name for layer in self._layers]:
            name += " 2"
        self._layers_named += 1
        return name

    def is_name_ok(self, name: str) -> bool:
        return (
            name.strip() != "" and
            name not in [layer.name for layer in self._layers]
        )

    def _on_new_file_event(self, _data: tuple):
        self._layers_named = 0
        self.map_maker.event.emit(MAP_SIZE_SET, (0, 0))

    def _on_map_size_set_event(self, data: tuple):
        map_w, map_h = data
        self.tell_size(map_w, map_h)

    def _render_layer(self, ren_x: int, ren_y: int, ren_w: int, ren_h: int):
        for layer in self._layers:
            if not layer.is_hidden():
                layer.render(ren_x, ren_y, ren_w, ren_h)

    def mouse_do_action(self, mouse_x: int, mouse_y: int):
        if self._layer is None or self._layer.is_hidden(): return
        self._draw_tiles(
            self._layer.texel_x_to_tile_x(self.real_x_to_texel_x(mouse_x)),
            self._layer.texel_y_to_tile_y(self.real_y_to_texel_y(mouse_y))
        )

    def mouse_stop_action(self, _mouse_x: int, _mouse_y: int):
        if self._draw_tiles_cmd is not None and not self._draw_tiles_cmd.is_empty:
            self._draw_tiles_cmd.layer.cmd.add(self._draw_tiles_cmd)
            self._draw_tiles_cmd = None

    def get_cursor_tile_pos(self, mouse_x: int, mouse_y: int) -> tuple:
        if self._layer is None or self._layer.is_hidden(): return None
        tile_x = self._layer.texel_x_to_tile_x(self.real_x_to_texel_x(mouse_x))
        tile_y = self._layer.texel_y_to_tile_y(self.real_y_to_texel_y(mouse_y))
        if self._layer.get_tile(tile_x, tile_y) == -1: return None
        return (tile_x, tile_y)

    def _draw_tiles(self, tile_x: int, tile_y: int):
        if self._layer is None or self._layer.is_hidden(): return
        tex_layer = self._layer.get_texture_layer()
        if tex_layer is None: return

        # check if we already set the requested tile there
        # or the tile doesn't exist
        tile_i = tex_layer.layer_man.get_selected_tile()
        if self._layer.get_tile(tile_x, tile_y) in (tile_i, -1):
            return

        if self._draw_tiles_cmd is None:
            self._draw_tiles_cmd = DrawTilesCommand(self._layer, tile_i)

        self._draw_tiles_cmd.add_line(tile_x, tile_y)

    def _run_length_encode_map(self, tile_map: list) -> list:
        rows = []
        for tiles in tile_map:
            row = []
            prev_tile = tiles[0]
            num = -1
            for tile in tiles + [None]:
                num += 1
                if tile != prev_tile:
                    row.append(f"{num}x{prev_tile}")
                    num = 0
                prev_tile = tile
            rows.append(" ".join(row))
        return rows

    def _run_length_decode_map(self, rle_map: list) -> list:
        tile_map = []
        for rle_row in rle_map:
            row = []
            for rle_data in rle_row.split(" "):
                num, tile = rle_data.split("x")
                tile = int(tile)
                row += [tile for _ in range(int(num))]
            tile_map.append(row)
        return tile_map

    def save(self) -> dict:
        layers = []
        for layer in self._layers:
            layer_data = layer.save()
            if not layer_data["positions"]:
                layer_data["tiles"] = self._run_length_encode_map(layer_data["tiles"])
            layers.append(layer_data)
        return {"layers": layers}

    def load(self, data: dict):
        self._layers_named = 0
        for layer_data in data["layers"]:
            tiles = layer_data["tiles"]
            if layer_data["positions"]:
                tiles = DrawLayer.positions_to_tile_map(tiles, layer_data["tile_size"], *data["map_size"])
            else:
                tiles = self._run_length_decode_map(tiles)
            draw_layer = DrawLayer(self,
                layer_data["tile_size"], layer_data["src_tex_name"],
                tiles, name=layer_data["name"]
            )
            draw_layer.set_save_positions(layer_data["positions"])
            self.add_layer(draw_layer)
            self._layers_named += 1
