from sgtk.Painter import Painter

from events import NEW_MAP
from BackgroundLayer import BackgroundLayer
from Dirtiness import Dirtiness

class LayerManager:
    def __init__(self, map_maker):
        self.map_maker = map_maker
        self.paint: Painter = map_maker.win.paint

        self._layer = None # Layer

        self.x = 0.0
        self.y = 0.0
        self.w = 0
        self.h = 0
        self.ren_w = 0.0
        self.ren_h = 0.0

        self._drag_off_x = 0.0
        self._drag_off_y = 0.0

        self.scale = 1.0
        self._min_scale = 1.0
        self.mouse_scroll_zoom_factor = 0.1

        self._hover_rect: tuple = None
        self._hover_color = (255, 255, 255, 130)

        self._bg_layer: BackgroundLayer = None

        self.map_maker.event.listen(NEW_MAP, self._on_new_map_event)

    def reset(self):
        self.x = 0.0
        self.y = 0.0
        self.w = 0
        self.h = 0
        self.scale = 1.0

        if self._bg_layer is not None:
            self._bg_layer.close()
            self._bg_layer = None

    def set_layer(self, layer):
        self._layer = layer

    def get_layer(self): return self._layer

    def tell_size(self, w: int, h: int):
        if 0 in (w, h): return
        elif (w, h) == (self.w, self.h): return
        self.w = w
        self.h = h
        self.ren_w = w
        self.ren_h = h

        min_side_size = 30
        self._min_scale = min_side_size / min(self.w, self.h)

        if self._bg_layer is not None: self._bg_layer.close()
        self._bg_layer = BackgroundLayer(self)
        Dirtiness.change()

    def mouse_clicked(self, mouse_x: int, mouse_y: int): pass
    def mouse_start_action(self, mouse_x: int, mouse_y: int): pass
    def mouse_do_action(self, mouse_x: int, mouse_y: int): pass
    def mouse_stop_action(self, mouse_x: int, mouse_y: int): pass

    def mouse_start_drag(self, mouse_x: int, mouse_y: int):
        self._drag_off_x = mouse_x - self.x
        self._drag_off_y = mouse_y - self.y

    def mouse_drag(self, mouse_x: int, mouse_y: int):
        self.x = mouse_x - self._drag_off_x
        self.y = mouse_y - self._drag_off_y

    def mouse_scroll_zoom(self, mouse_x: int, mouse_y: int, scroll_dy: int):
        self.change_zoom(
            1 + scroll_dy * self.mouse_scroll_zoom_factor,
            mouse_x, mouse_y
        )

    def set_zoom(self, scale: float, target_real_x: int, target_real_y: int):
        target_texel_x = self.real_x_to_texel_x(target_real_x)
        target_texel_y = self.real_y_to_texel_y(target_real_y)

        self.scale = max(self._min_scale, scale)
        self.ren_w = self.w * self.scale
        self.ren_h = self.h * self.scale

        self.x = target_real_x - self.scale * target_texel_x
        self.y = target_real_y - self.scale * target_texel_y

    def change_zoom(self, delta_scale: float, target_real_x: int, target_real_y: int):
        self.set_zoom(self.scale * delta_scale, target_real_x, target_real_y)

    def real_x_to_texel_x(self, real_x: int) -> float:
        return (real_x - self.x) / self.scale
    def real_y_to_texel_y(self, real_y: int) -> float:
        return (real_y - self.y) / self.scale

    def close(self):
        self.map_maker.event.unlisten(NEW_MAP, self._on_new_map_event)
        if self._layer is not None: self._layer.close()
        if self._bg_layer is not None: self._bg_layer.close()

    def render(self):
        x, y, w, h = self._get_render_rect()

        self._render_background(x, y, w, h)
        self._render_layer(x, y, w, h)
        self._render_hover_rect(self._hover_color, self._hover_rect, x, y)

    def _get_render_rect(self) -> tuple:
        return (int(self.x), int(self.y), int(self.ren_w), int(self.ren_h))

    def _render_layer(self, ren_x: int, ren_y: int, ren_w: int, ren_h: int):
        if self._layer is not None: self._layer.render(ren_x, ren_y, ren_w, ren_h)

    def _render_background(self, ren_x: int, ren_y: int, ren_w: int, ren_h: int):
        if self._bg_layer is not None: self._bg_layer.render(ren_x, ren_y, ren_w, ren_h)

    def _render_hover_rect(self, color: tuple, hover_rect: tuple, ren_x: int, ren_y: int):
        if not hover_rect: return
        hx, hy, hw, hh = hover_rect
        self.paint.rect(color,
            ren_x + int(hx * self.scale), ren_y + int(hy * self.scale),
            int(hw * self.scale), int(hh * self.scale)
        )

    def update_hover_rect(self, mouse_x: int, mouse_y: int):
        self._hover_rect = None
        if self._layer is None or (mouse_x == mouse_y == -1) : return

        self._hover_rect = self._layer.get_tile_rect_from_texel(
            int(self.real_x_to_texel_x(mouse_x)), int(self.real_y_to_texel_y(mouse_y))
        )

    def _on_new_map_event(self, _data: None): self.reset()
