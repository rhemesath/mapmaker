from sgtk import SGTK

from dialogs.YesOrCancelDialog import YesOrCancelDialog

class WarnUnsaved(YesOrCancelDialog):
    def __init__(self, sgtk: SGTK, callback_on_ok:callable=None, callback:callable=None):
        super().__init__(sgtk,
            "Warning",
            "There are unsaved changes. Are you sure you want to discard them?",
            callback_on_ok=callback_on_ok, callback=callback
        )
