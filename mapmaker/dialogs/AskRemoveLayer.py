from sgtk import SGTK

from dialogs.YesOrCancelDialog import YesOrCancelDialog

class AskRemoveLayer(YesOrCancelDialog):
    def __init__(self, sgtk: SGTK, layer_name: str, callback_on_ok:callable=None, callback:callable=None):
        super().__init__(sgtk,
            "Remove Layer?",
            f'Do you really want to delete the layer "{layer_name}"?',
            callback_on_ok=callback_on_ok, callback=callback
        )
