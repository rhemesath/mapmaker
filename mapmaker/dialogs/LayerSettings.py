from sdl2 import SDLK_ESCAPE
from sgtk import (
    Window, Frame, Label, Button, TextInputLine
)
from sgtk.constants import KEYBOARD_PRESS_KEY

from DrawLayer import DrawLayer
from theme import dialog_theme, WIDGET_COLOR

class LayerSettings:
    def __init__(self, draw_layer: DrawLayer):
        self.draw_layer = draw_layer
        self.tk = draw_layer.map_maker.tk
        self._draw_layer_man = draw_layer.layer_man

        self._setup_window()

    def _setup_window(self):
        win = Window(self.tk, title=f'"{self.draw_layer.name}" Settings',
            num_rows=2, **dialog_theme["window"]
        )
        def win_on_keyboard_press_key(sdl2_keycode: int):
            if sdl2_keycode == SDLK_ESCAPE: win.on_close_request()
        win.event.listen(KEYBOARD_PRESS_KEY, win_on_keyboard_press_key)

        # Main Input
        name_label = Label(win, text="Layer name", **dialog_theme["main_text"])
        name_input_line = TextInputLine(win, text=self.draw_layer.name, **dialog_theme["main_input"])

        store_type_label = Label(win, text="Store content as", **dialog_theme["main_text"])
        store_type_tiles_btn = Button(win, text="Tile map", **dialog_theme["main_btn"])
        store_type_positions_btn = Button(win, text="Entity positions", **dialog_theme["main_btn"])

        def store_type_tiles_btn_cmd():
            store_type_tiles_btn.deactivate()
            store_type_positions_btn.activate()
        def store_type_positions_btn_cmd():
            store_type_positions_btn.deactivate()
            store_type_tiles_btn.activate()
        store_type_tiles_btn.set_cmd(store_type_tiles_btn_cmd)
        store_type_positions_btn.set_cmd(store_type_positions_btn_cmd)

        if self.draw_layer.does_save_positions(): store_type_positions_btn_cmd()
        else: store_type_tiles_btn_cmd()

        store_type_frame = Frame(win, num_cols=2, **{**dialog_theme["main_frame"], "color": WIDGET_COLOR, "pad_x": 4, "pad_y": 4})
        store_type_frame.add_grid_child(store_type_tiles_btn,     col=0)
        store_type_frame.add_grid_child(store_type_positions_btn, col=1)

        main_widgets = [
            (name_label, name_input_line),
            (store_type_label, store_type_frame)
        ]
        main_frame = Frame(win,
            num_cols=len(main_widgets[0]), num_rows=len(main_widgets), **dialog_theme["main_frame"]
        )
        for row_i, widgets in enumerate(main_widgets):
            for col_i, widget in enumerate(widgets):
                main_frame.add_grid_child(widget, col=col_i, row=row_i)

        ok_btn     = Button(win, text="Ok", **dialog_theme["action_btn"])
        cancel_btn = Button(win, text="Cancel", cmd=win.on_close_request, **dialog_theme["action_btn"])

        def is_valid() -> bool:
            name = name_input_line.get_text()
            if self._draw_layer_man.is_name_ok(name): return True
            if name == self.draw_layer.name: return True
            return False

        def ok():
            if not is_valid(): return
            layer_name = name_input_line.get_text()
            if layer_name != self.draw_layer.name:
                self.draw_layer.change_name(layer_name)

            saves_positions = not store_type_positions_btn.is_active()
            self.draw_layer.set_save_positions(saves_positions)

            win.on_close_request()
        ok_btn.set_cmd(ok)

        def update_ok_btn_on_input():
            if is_valid(): ok_btn.activate()
            else: ok_btn.deactivate()
        name_input_line.validate = update_ok_btn_on_input
        name_input_line.on_key_enter = ok

        # Action Frame
        action_frame = Frame(win, num_cols=2, **dialog_theme["action_frame"])
        action_frame.add_grid_child(cancel_btn, col=0)
        action_frame.add_grid_child(ok_btn,     col=1)

        win.add_grid_child(main_frame,   row=0)
        win.add_grid_child(action_frame, row=1)

        self.tk.add_child(win)
