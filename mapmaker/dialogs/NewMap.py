from sdl2 import SDLK_ESCAPE
from sgtk import (
    Window, Frame, Label, Button, TextInputLine
)
from sgtk.constants import KEYBOARD_PRESS_KEY
from sgtk.TextInputLine import TEXT_INPUT_FILTER_POSITIVE_INT

from theme import dialog_theme
from events import NEW_MAP, MAP_SIZE_SET

class NewMap:
    def __init__(self, map_maker):
        self.tk = map_maker.tk
        self.map_maker = map_maker
        self._win: Window = None

        self._setup_window()

    def get_window(self) -> Window:
        return self._win

    def _setup_window(self):
        self._win = Window(self.tk, title="New Map...", num_rows=2, **dialog_theme["window"])
        def win_on_keyboard_press_key(sdl2_keycode: int):
            if sdl2_keycode == SDLK_ESCAPE: self._win.on_close_request()
        self._win.event.listen(KEYBOARD_PRESS_KEY, win_on_keyboard_press_key)

        # Action buttons
        cancel_btn = Button(self._win, text="Cancel", cmd=self._win.on_close_request, **dialog_theme["action_btn"])
        ok_button  = Button(self._win, text="OK", **dialog_theme["action_btn"])

        action_frame = Frame(self._win, num_cols=2, **dialog_theme["action_frame"])
        action_frame.add_grid_child(cancel_btn, col=0)
        action_frame.add_grid_child(ok_button,  col=1)

        # Main input lines
        main_input_theme = {**dialog_theme["main_input"], "min_w": 100} # make a bit smaller

        tile_size_label      = Label(self._win, text="Tile size", **dialog_theme["main_text"])
        tile_size_input_line = TextInputLine(self._win, text="16", input_filter=TEXT_INPUT_FILTER_POSITIVE_INT, **main_input_theme)
        map_w_label      = Label(self._win, text="Map width", **dialog_theme["main_text"])
        map_w_input_line = TextInputLine(self._win, input_filter=TEXT_INPUT_FILTER_POSITIVE_INT, **main_input_theme)
        map_h_label      = Label(self._win, text="Map height", **dialog_theme["main_text"])
        map_h_input_line = TextInputLine(self._win, input_filter=TEXT_INPUT_FILTER_POSITIVE_INT, **main_input_theme)

        grid = [
            (tile_size_label, tile_size_input_line),
            (map_w_label,     map_w_input_line),
            (map_h_label,     map_h_input_line)
        ]
        main_frame = Frame(self._win, num_cols=len(grid[0]), num_rows=len(grid), **dialog_theme["main_frame"])
        for row, widgets in enumerate(grid):
            for col, widget in enumerate(widgets):
                main_frame.add_grid_child(widget, col=col, row=row)

        input_lines = [input_row[1] for input_row in grid]
        def is_valid() -> bool:
            for input_line in input_lines:
                text = input_line.get_text()
                if text == "" or int(text) == 0: return False
            return True

        def ok():
            if not is_valid(): return
            tile_size = int(tile_size_input_line.get_text())
            map_w = int(map_w_input_line.get_text())
            map_h = int(map_h_input_line.get_text())

            self.map_maker.event.emit(NEW_MAP, None)
            self.map_maker.event.emit(MAP_SIZE_SET, (map_w * tile_size, map_h * tile_size))
            self._win.on_close_request()
        ok_button.set_cmd(ok)

        # Add some final logic to the input lines
        def update_ok_btn_on_input():
            if is_valid(): ok_button.activate()
            else: ok_button.deactivate()
        for i in range(len(input_lines)):
            input_lines[i].set_next(input_lines[(i + 1) % len(input_lines)]) # set next
            input_lines[i].validate = update_ok_btn_on_input
        input_lines[-1].on_key_enter = ok

        # add to window
        self._win.add_grid_child(main_frame,   row=0)
        self._win.add_grid_child(action_frame, row=1)
        self.tk.add_child(self._win)

        update_ok_btn_on_input()
        tile_size_input_line.activate()
