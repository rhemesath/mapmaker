from sdl2 import SDLK_ESCAPE
from sgtk import (
    Window, Frame, Label, Button, TextInputLine
)
from sgtk.constants import KEYBOARD_PRESS_KEY
from sgtk.TextInputLine import TEXT_INPUT_FILTER_POSITIVE_INT

from DrawLayer import DrawLayer
from theme import dialog_theme

class NewLayer:
    def __init__(self, map_maker):
        self.tk = map_maker.tk
        self.draw_layer_man = map_maker.draw_layer_man

        self._setup_window()

    def _setup_window(self):
        win = Window(self.tk, title="Create Layer...", num_rows=2, **dialog_theme["window"])
        def win_on_keyboard_press_key(sdl2_keycode: int):
            if sdl2_keycode == SDLK_ESCAPE: win.on_close_request()
        win.event.listen(KEYBOARD_PRESS_KEY, win_on_keyboard_press_key)

        # Action buttons
        cancel_btn = Button(win, text="Cancel", cmd=win.on_close_request, **dialog_theme["action_btn"])
        create_btn = Button(win, text="Create", **dialog_theme["action_btn"])

        action_frame = Frame(win, num_cols=2, **dialog_theme["action_frame"])
        action_frame.add_grid_child(cancel_btn, col=0)
        action_frame.add_grid_child(create_btn, col=1)

        # Main input lines
        tile_size_label = Label(win, text="Tile size", **dialog_theme["main_text"])
        tile_size_input_line = TextInputLine(win, text="16", input_filter=TEXT_INPUT_FILTER_POSITIVE_INT, **dialog_theme["main_input"])
        texture_name_label = Label(win, text="Texture name", **dialog_theme["main_text"])
        texture_name_input_line = TextInputLine(win, text="Texture 0", **dialog_theme["main_input"])

        main_frame = Frame(win, num_cols=2, num_rows=2, **dialog_theme["main_frame"])
        for row, widgets in enumerate([
            (tile_size_label, tile_size_input_line),
            (texture_name_label, texture_name_input_line)
        ]):
            for col, widget in enumerate(widgets):
                main_frame.add_grid_child(widget, col=col, row=row)

        def is_valid() -> bool:
            ts_str = tile_size_input_line.get_text()
            if ts_str == "": return False
            ts = int(ts_str)
            if ts == 0: return False

            text = texture_name_input_line.get_text()
            if text == "": return False

            if (self.draw_layer_man.w % ts != 0 or
                self.draw_layer_man.h % ts != 0): return False

            return True

        def create():
            if not is_valid(): return
            tile_size = int(tile_size_input_line.get_text())
            texture_name = texture_name_input_line.get_text()
            self.draw_layer_man.add_layer(DrawLayer(self.draw_layer_man, tile_size, texture_name))
            win.on_close_request()
        create_btn.set_cmd(create)

        # Add some final logic to the input lines
        def update_create_btn_on_input():
            if is_valid(): create_btn.activate()
            else: create_btn.deactivate()
        tile_size_input_line.set_next(texture_name_input_line)
        tile_size_input_line.validate = update_create_btn_on_input
        texture_name_input_line.set_next(tile_size_input_line)
        texture_name_input_line.validate = update_create_btn_on_input
        texture_name_input_line.on_key_enter = create

        # Add to window
        win.add_grid_child(main_frame,   row=0)
        win.add_grid_child(action_frame, row=1)
        self.tk.add_child(win)

        update_create_btn_on_input()
        tile_size_input_line.activate()
