from sgtk import SGTK, AskYesOrCancel
from theme import yes_or_cancel_dialog_theme

class FileAlreadyExists(AskYesOrCancel):
    def __init__(self, sgtk: SGTK, title:str="Title", text:str="Text"):
        super().__init__(sgtk, title=title, text=text, **yes_or_cancel_dialog_theme)
