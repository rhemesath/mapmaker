from sgtk import SGTK, AskYesOrCancel
from sgtk.constants import USER_DECISION

from theme import yes_or_cancel_dialog_theme

class YesOrCancelDialog(AskYesOrCancel):
    def __init__(self, sgtk: SGTK, title: str, text: str,
        callback_on_ok:callable=None, callback:callable=None
    ):
        super().__init__(sgtk, title=title, text=text, **yes_or_cancel_dialog_theme)
        self._callback_on_ok = callback_on_ok
        self._callback = callback
        self.tk.event.listen(USER_DECISION, self._on_user_decision)

    def _on_user_decision(self, dialog: AskYesOrCancel):
        if dialog is not self: return
        self.tk.event.unlisten(USER_DECISION, self._on_user_decision)
        if self.get_decision() and self._callback_on_ok is not None:
            self._callback_on_ok()
        if self._callback is not None: self._callback()
