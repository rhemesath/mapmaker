from sdl2 import SDLK_SPACE
from sgtk import TextureWidget
from sgtk.constants import (
    MOUSE_WHEEL_Y, MOUSE_WHEEL_X, MOUSE_DOWN, MOUSE_UP,
    MOUSE_LEFT, KEYBOARD_PRESS_KEY, KEYBOARD_RELEASE_KEY
)

from MapWidgetInput import MapWidgetInput

class MapWidgetTouchpadInput(MapWidgetInput):
    def __init__(self, map_widget: TextureWidget,
        action_key:int=SDLK_SPACE, zoom_key:str="ctrl", drag_factor:int=10,
        flip_x:bool=False, flip_y:bool=False
    ):
        super().__init__(map_widget)
        self.action_key = action_key
        self.zoom_key = zoom_key
        self.drag_factor = drag_factor
        self.flip_x = flip_x
        self.flip_y = flip_y

        self.mouse = self._map_widget.tk.mouse
        self.keyboard = self._map_widget.tk.keyboard

        self._events = {
            MOUSE_DOWN:    self.on_mouse_down_event,
            MOUSE_UP:      self.on_mouse_up_event,
            MOUSE_WHEEL_X: self.on_mouse_scroll_x,
            MOUSE_WHEEL_Y: self.on_mouse_scroll_y,
            KEYBOARD_PRESS_KEY:   self.on_keyboard_press_key,
            KEYBOARD_RELEASE_KEY: self.on_keyboard_release_key
        }

        self._add_event_listeners()

    def on_mouse_down_event(self, button: int):
        if self.mouse.selected is not self._map_widget: return
        self._map_widget.set_active(True)
        if button == MOUSE_LEFT:
            self._map_widget.layer_man.mouse_clicked(*self._map_widget.get_rel_mouse_pos())

    def on_mouse_up_event(self, _button: int):
        self._map_widget.set_active(False)

    def on_keyboard_press_key(self, sdl2_keycode: int):
        if not self._map_widget.is_active() or self.mouse.selected is not self._map_widget:
            return
        if sdl2_keycode == self.action_key:
            self._map_widget.layer_man.mouse_start_action(*self._map_widget.get_rel_mouse_pos())

    def on_keyboard_release_key(self, sdl2_keycode: int):
        if sdl2_keycode == self.action_key:
            self._map_widget.layer_man.mouse_stop_action(*self._map_widget.get_rel_mouse_pos())

    def on_mouse_scroll_x(self, delta_scroll_x: int):
        if self.mouse.selected is not self._map_widget: return
        layer_man = self._map_widget.layer_man
        layer_man.x += round(delta_scroll_x * self.drag_factor * layer_man.scale * (-1 if self.flip_x else 1))

    def on_mouse_scroll_y(self, delta_scroll_y: int):
        if self.mouse.selected is not self._map_widget: return
        layer_man = self._map_widget.layer_man
        if self.keyboard.get_mod(self.zoom_key):
            layer_man.mouse_scroll_zoom(*self._map_widget.get_rel_mouse_pos(), delta_scroll_y)
        else:
            layer_man.y -= round(delta_scroll_y * self.drag_factor * layer_man.scale * (-1 if self.flip_y else 1))

    def on_update(self):
        layer_man = self._map_widget.layer_man

        if self.mouse.selected is self._map_widget:
            mouse_x, mouse_y = self._map_widget.get_rel_mouse_pos()
            if self.keyboard.is_pressed(self.action_key):
                layer_man.mouse_do_action(mouse_x, mouse_y)
            layer_man.update_hover_rect(mouse_x, mouse_y)
        else:
            layer_man.update_hover_rect(-1, -1)
