from sgtk import TextureWidget
from sgtk.constants import (
    MOUSE_MOTION, MOUSE_WHEEL_Y, MOUSE_DOWN, MOUSE_UP,
    MOUSE_LEFT, MOUSE_RIGHT
)

from MapWidgetInput import MapWidgetInput

class MapWidgetMouseInput(MapWidgetInput):
    def __init__(self, map_widget: TextureWidget,
        action_button:int=MOUSE_LEFT, drag_button:int=MOUSE_RIGHT
    ):
        super().__init__(map_widget)
        self.action_button = action_button
        self.drag_button   = drag_button

        self.mouse = self._map_widget.tk.mouse

        self._events = {
            MOUSE_DOWN:    self.on_mouse_down_event,
            MOUSE_UP:      self.on_mouse_up_event,
            MOUSE_MOTION:  self.on_mouse_move_event,
            MOUSE_WHEEL_Y: self.on_mouse_scroll_y
        }

        self._add_event_listeners()

    def on_mouse_down_event(self, button: int):
        if self.mouse.selected is not self._map_widget: return
        self._map_widget.set_active(True)
        if button == MOUSE_LEFT:
            self._map_widget.layer_man.mouse_clicked(*self._map_widget.get_rel_mouse_pos())
        if button == self.drag_button:
            self._map_widget.layer_man.mouse_start_drag(*self._map_widget.get_rel_mouse_pos())
        elif button == self.action_button:
            self._map_widget.layer_man.mouse_start_action(*self._map_widget.get_rel_mouse_pos())

    def on_mouse_up_event(self, button: int):
        if self._map_widget.is_active():
            if button == self.action_button:
                self._map_widget.layer_man.mouse_stop_action(*self._map_widget.get_rel_mouse_pos())
        self._map_widget.set_active(False)

    def on_mouse_move_event(self, _):
        if self._map_widget.is_active() and self.mouse.is_pressed(self.drag_button):
            self._map_widget.layer_man.mouse_drag(*self._map_widget.get_rel_mouse_pos())

    def on_mouse_scroll_y(self, delta_scroll_y: int):
        if self.mouse.selected is not self._map_widget: return
        self._map_widget.layer_man.mouse_scroll_zoom(
            *self._map_widget.get_rel_mouse_pos(), delta_scroll_y
        )

    def on_update(self):
        layer_man = self._map_widget.layer_man

        if self.mouse.selected is self._map_widget:
            mouse_x, mouse_y = self._map_widget.get_rel_mouse_pos()
            if self.mouse.is_pressed(self.action_button):
                layer_man.mouse_do_action(mouse_x, mouse_y)
            layer_man.update_hover_rect(mouse_x, mouse_y)
        else:
            layer_man.update_hover_rect(-1, -1)
