from abc import ABC, abstractmethod
from sgtk import TextureWidget

class MapWidgetInput(ABC):
    def __init__(self, map_widget: TextureWidget):
        self._map_widget = map_widget
        self._event_man = map_widget.win.event

        self._events = {}

    def _add_event_listeners(self):
        for event_id, callback in self._events.items():
            self._event_man.listen(event_id, callback)

    def close(self):
        for event_id, callback in self._events.items():
            self._event_man.unlisten(event_id, callback)

    @abstractmethod
    def on_update(self):
        raise NotImplementedError()
