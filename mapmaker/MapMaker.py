from pathlib import Path
import json
from sdl2 import (
    SDLK_z, SDLK_y, SDLK_n, SDLK_w, SDLK_a, SDLK_s, SDLK_d,
    SDLK_l, SDLK_t, SDLK_PLUS, SDLK_UP, SDLK_DOWN, SDLK_h,
    SDLK_g, SDLK_F5, SDLK_MINUS
)

import sgtk
from sgtk.EventManager import EventManager
from sgtk.constants import PATH_OPEN, KEYBOARD_PRESS_KEY, WINDOW_CLOSED, MOUSE_MOTION

from MapWidget import MapWidget
from theme import (
    WIN_BASE_COLOR, WIDGET_COLOR, WIN_CONTENT_COLOR, TEXT_COLOR,
    WIDGET_BRIGHT_COLOR, TEXT_BRIGHT_COLOR, HIGHLIGHT_COLOR, button_theme, explorer_theme
)
from events import (
    CMD_MAN_UPDATE, NEW_MAP, MAP_SIZE_SET, DRAW_LAYER_SET,
    DRAW_LAYER_ADDED, DRAW_LAYER_REMOVED, DRAW_LAYER_MOVED,
    DRAW_LAYER_HIDDEN, TEXTURE_UPDATED, DIRTINESS_CHANGED,
    REFRESH_DRAW_LAYERS, REFRESH_TEXTURES, DRAW_LAYER_NAME_SET
)

from Restorable import Restorable
from Dirtiness import Dirtiness
from LayerManager import LayerManager
from TextureLayerCollection import TextureLayerCollection
from DrawLayerManager import DrawLayerManager
from DrawLayer import DrawLayer
from MapWidgetMouseInput import MapWidgetMouseInput

from dialogs import (
    NewMap, NewLayer, FileAlreadyExists, WarnUnsaved, AskRemoveLayer,
    LayerSettings
)

class MapMaker(Restorable):
    def __init__(self,
        accelerated:bool=False,
        window_scale:float=1.0,
        MapWidgetInputFactory=MapWidgetMouseInput
    ):
        self._accelerated = accelerated
        self._window_scale = window_scale
        self._MapWidgetInputFactory = MapWidgetInputFactory

        self._title = "MapMaker"

        self.tk: sgtk.SGTK = None
        self.win: sgtk.Window = None
        self.map_widget: MapWidget = None
        self.draw_layer_man: DrawLayerManager = None
        self.texture_map_widget: MapWidget = None
        self.texture_collection = TextureLayerCollection(self)

        self.this_dir = Path(__file__).parent.resolve()
        self.event = EventManager()

        self._open_texture_dialog: sgtk.OpenFile = None
        self._add_texture_btn: sgtk.Button = None

        self._new_map_dialog: NewMap   = None
        self._new_map_btn: sgtk.Button = None
        self._last_save_path: Path       = None
        self._save_dialog: sgtk.SaveFile = None
        self._save_map_btn: sgtk.Button  = None
        self._load_dialog: sgtk.OpenFile = None
        self._load_map_btn: sgtk.Button  = None

        self._warn_unsaved_dialog: WarnUnsaved = None
        self._ask_remove_layer_dialog: AskRemoveLayer = None

        self.event.listen(NEW_MAP, self._on_new_map)
        self._setup_window()

        Dirtiness.setup(self)

        self.event.emit(NEW_MAP, None)

    def run(self):
        self.tk.run()

    def sgtk_update(self):
        self.map_widget.update()
        self.texture_map_widget.update()

    # TODO
    def undo(self):
        draw_layer = self.draw_layer_man.get_layer()
        if draw_layer is not None: draw_layer.cmd.undo()
    def redo(self):
        draw_layer = self.draw_layer_man.get_layer()
        if draw_layer is not None: draw_layer.cmd.redo()
    def toggle_draw_layer_hidden(self, draw_layer: DrawLayer):
        """ draw_layer: Optional[DrawLayer] """
        if draw_layer is not None: draw_layer.set_hidden(not draw_layer.is_hidden())

    def load(self, data: dict):
        file_path = self._last_save_path
        self.event.emit(NEW_MAP, None)
        self.event.emit(MAP_SIZE_SET, tuple(data["map_size"]))

        # make texture paths relative to the file
        texture_data = data["textures"]
        for name, path in texture_data.items():
            if path is None: continue
            path = Path(path)
            if path.is_absolute(): continue
            texture_data[name] = str(file_path.parent.joinpath(path))

        self.texture_collection.load(data)
        self.draw_layer_man.load(data)

    def save(self) -> dict:
        layer_data = self.draw_layer_man.save()["layers"]
        source_texture_names = set(layer["src_tex_name"] for layer in layer_data)

        textures = self.texture_collection.save()
        texture_data = {}
        for name in source_texture_names:
            path = textures[name]
            if path is not None:
                path = str(Path(path).relative_to(self._last_save_path.parent))
            texture_data[name] = path

        data = {
            "map_size": [self.draw_layer_man.w, self.draw_layer_man.h],
            "textures": texture_data,
            "layers": layer_data
        }

        return data

    def save_file(self):
        if self._last_save_path is None: self.save_file_as()
        else: self._save_file()

    def load_file(self):
        if self._load_dialog is not None: return # Load Map window is already open
        if Dirtiness.get():
            self._warn_if_unsaved(self._ask_load_file)
        else: self._ask_load_file()

    def _setup_window(self):
        self.tk = sgtk.SGTK(accelerated=self._accelerated, window_scale=self._window_scale)
        self.tk.should_close = self._on_tk_close_request
        self.tk.event.listen(WINDOW_CLOSED, self._on_window_closed)

        self.win = sgtk.Window(self.tk, self._title, num_rows=2, color=WIN_BASE_COLOR, debug_name="win")
        def win_on_close_request():
            if Dirtiness.get():
                self._warn_if_unsaved(self.tk.stop)
            else:
                self.tk.stop()
        self.win.on_close_request = win_on_close_request

        self.event.listen(DIRTINESS_CHANGED, lambda _dirtiness: self._update_title())

        self.draw_layer_man = DrawLayerManager(self)

        menu_bar = self._create_menu_bar()
        win_frame = self._create_win_frame()

        self.win.add_grid_child(menu_bar,  row=0)
        self.win.add_grid_child(win_frame, row=1)

        self.win.event.listen(KEYBOARD_PRESS_KEY, self._on_key_press)
        self.win.event.listen(MOUSE_MOTION, self._update_title)

        self.tk.add_child(self.win)

        self.tk.update = self.sgtk_update

    def _create_menu_bar(self) -> sgtk.Frame:
        kwargs = {
            "ipad_x": 8, "color": TEXT_COLOR, "bg_color": WIN_BASE_COLOR, "hl_bg_color": HIGHLIGHT_COLOR,
            "deactive_bg_color": WIN_BASE_COLOR
        }

        self._new_map_btn = sgtk.Button(self.win, text="New Map", cmd=self._new_map, **kwargs)
        self._save_map_btn = sgtk.Button(self.win, text="Save Map", cmd=self.save_file, **kwargs)
        def save_map_btn_on_path_open(save_dialog):
            if save_dialog is not self._save_dialog: return

            path = self._save_dialog.get_path()
            self._save_dialog = None # remember that the SaveFile window is closed
            self._save_map_btn.activate() # allow to use the "Save Map" button again

            if path is None: return # user canceled
            self._last_save_path = path.resolve()
            self._update_title()
            self._save_file()
        self.tk.event.listen(PATH_OPEN, save_map_btn_on_path_open)

        self._load_map_btn = sgtk.Button(self.win, text="Load Map", cmd=self.load_file, **kwargs)
        def load_map_btn_on_path_open(open_dialog):
            if open_dialog is not self._load_dialog: return

            path = self._load_dialog.get_path()
            self._load_dialog = None # remember that the OpenFile window is closed
            self._load_map_btn.activate() # allow to use the "Load Map" button again

            if path is None: return # user canceled
            self._load_file(path)
        self.tk.event.listen(PATH_OPEN, load_map_btn_on_path_open)

        undo = sgtk.Button(self.win, text="Undo", cmd=self.undo, **kwargs)
        redo = sgtk.Button(self.win, text="Redo", cmd=self.redo, **kwargs)
        undo.deactivate(); redo.deactivate()

        def on_command_manager_update(data: tuple):
            last_undo, last_redo = data
            if last_undo: undo.deactivate()
            else: undo.activate()
            if last_redo: redo.deactivate()
            else: redo.activate()
        self.event.listen(CMD_MAN_UPDATE, on_command_manager_update)

        buttons = (self._new_map_btn, self._load_map_btn, self._save_map_btn, undo, redo)

        menu_bar = sgtk.Frame(self.win, num_cols=len(buttons), debug_name="menu_bar")
        for col, button in enumerate(buttons):
            menu_bar.add_grid_child(button, col=col)

        return menu_bar

    def _create_win_frame(self) -> sgtk.Frame:
        main_frame = self._create_main_frame()
        side_frame = self._create_side_bar()

        win_frame = sgtk.Frame(self.win, num_cols=2, debug_name="win_frame", color=WIN_CONTENT_COLOR)

        win_frame.add_grid_child(main_frame, col=0)
        win_frame.add_grid_child(side_frame, col=1)

        return win_frame

    def _create_main_frame(self) -> sgtk.Frame:
        self.map_widget = MapWidget(self, 850, 680, self.draw_layer_man, bg_color=WIDGET_COLOR, debug_name="map_widget")
        self.map_widget.set_input(self._MapWidgetInputFactory(self.map_widget))

        main_frame = sgtk.Frame(self.win, debug_name="main_frame")
        main_frame.add_grid_child(self.map_widget)

        return main_frame

    def _create_side_bar(self) -> sgtk.Frame:
        rows = [
            self._create_texture_frame(),
            self._create_layer_frame()
        ]

        side_frame = sgtk.Frame(self.win, num_rows=len(rows), color=WIN_CONTENT_COLOR, pad_x=2, debug_name="side_frame")
        for row, widget in enumerate(rows):
            side_frame.add_grid_child(widget, row=row)

        return side_frame

    def _create_texture_frame(self) -> sgtk.Frame:
        # texture menu bar
        texture_label = sgtk.Label(self.win, bg_color=WIN_CONTENT_COLOR)

        self._add_texture_btn = sgtk.Button(self.win, text="+", cmd=self._add_texture, **button_theme)
        def on_texture_path_open(open_file: sgtk.OpenFile):
            if open_file is not self._open_texture_dialog: return
            self._add_texture_btn.activate()
            path = self._open_texture_dialog.get_path()
            self._open_texture_dialog = None
            if path is None: return
            draw_layer = self.draw_layer_man.get_layer()
            self.texture_collection.setup_layer(draw_layer.src_tex_name, draw_layer.ts, path)
        self.tk.event.listen(PATH_OPEN, on_texture_path_open)

        def update_tex_menu_bar(tex_name: str, tile_size: int):
            if self.texture_collection.get_layer(tex_name, tile_size) is None:
                tex_name += " (empty)"
                self._add_texture_btn.activate()
            else:
                self._add_texture_btn.deactivate()
            texture_label.set_text(tex_name)

        def on_draw_layer_set(data: tuple):
            new_draw_layer = data[1]
            if new_draw_layer is None:
                texture_label.set_text("[No texture]")
                self._add_texture_btn.deactivate()
            else:
                update_tex_menu_bar(new_draw_layer.src_tex_name, new_draw_layer.ts)

        def on_texture_update_event(name: str):
            # check if the texture with that name is currently shown
            draw_layer = self.draw_layer_man.get_layer()
            if draw_layer is None or draw_layer.src_tex_name != name: return
            update_tex_menu_bar(name, draw_layer.ts)

        self.event.listen(DRAW_LAYER_SET, on_draw_layer_set)
        self.event.listen(TEXTURE_UPDATED, on_texture_update_event)

        menu_bar_columns = [texture_label, self._add_texture_btn]

        texture_menu_bar = sgtk.Frame(self.win, num_cols=len(menu_bar_columns), color=WIN_CONTENT_COLOR, debug_name="texture_menu_bar")
        for column, menu_bar_widget in enumerate(menu_bar_columns):
            texture_menu_bar.add_grid_child(menu_bar_widget, col=column)

        # texture map widget
        self.texture_map_widget = MapWidget(self, 300, 300, LayerManager(self), bg_color=WIDGET_COLOR, pad_x=40, debug_name="texture_map_widget")
        self.texture_map_widget.set_input(self._MapWidgetInputFactory(self.texture_map_widget))
        def texture_map_widget_on_draw_layer_set_event(data: tuple):
            prev_draw_layer, new_draw_layer = data
            tex_layer_man = None
            if prev_draw_layer is not new_draw_layer and new_draw_layer is None:
                tex_layer_man = LayerManager(self)
            elif new_draw_layer is not None:
                tex_layer_man = self.texture_collection.get_manager(new_draw_layer.src_tex_name, new_draw_layer.ts)

            if tex_layer_man is not None:
                self.texture_map_widget.change_layer_manager(tex_layer_man)
        self.event.listen(DRAW_LAYER_SET, texture_map_widget_on_draw_layer_set_event)

        # texture frame
        texture_frame = sgtk.Frame(self.win, num_rows=2, color=WIN_CONTENT_COLOR, pad_y=2, debug_name="texture_frame")
        texture_frame.add_grid_child(texture_menu_bar,        row=0)
        texture_frame.add_grid_child(self.texture_map_widget, row=1)
        return texture_frame

    def _create_layer_frame(self):
        # general settings
        layers_label = sgtk.Label(self.win, text="Layers", bg_color=WIN_CONTENT_COLOR)
        add_layer = sgtk.Button(self.win, text="+", cmd=self._add_layer, **button_theme)
        def add_layer_on_map_size_set(data: tuple):
            if 0 in data: add_layer.deactivate()
            else: add_layer.activate()
        self.event.listen(MAP_SIZE_SET, add_layer_on_map_size_set)

        remove_layer = sgtk.Button(self.win, text="-", cmd=self._remove_layer, **button_theme)
        remove_layer.deactivate()
        def remove_layer_on_draw_layer_set(data: tuple):
            new_draw_layer = data[1]
            if new_draw_layer is None: remove_layer.deactivate()
            else: remove_layer.activate()
        self.event.listen(DRAW_LAYER_SET, remove_layer_on_draw_layer_set)

        move_layer_up = sgtk.Button(self.win, text="up", cmd=lambda: self._move_current_draw_layer(True), **button_theme)
        move_layer_down = sgtk.Button(self.win, text="down", cmd=lambda: self._move_current_draw_layer(False), **button_theme)
        def update_move_buttons():
            draw_layer = self.draw_layer_man.get_layer()
            if draw_layer is None:
                move_layer_up.deactivate()
                move_layer_down.deactivate()
                return
            row = self.draw_layer_man.get_layer_row(draw_layer)
            if row is None: return
            move_layer_up.activate()
            move_layer_down.activate()
            if row == 0: move_layer_down.deactivate()
            if row == self.draw_layer_man.get_num_layers() - 1: move_layer_up.deactivate()
        for event_type in (DRAW_LAYER_ADDED, DRAW_LAYER_REMOVED, DRAW_LAYER_MOVED, DRAW_LAYER_SET):
            self.event.listen(event_type, lambda data: update_move_buttons())

        settings_cols = [layers_label, add_layer, remove_layer, move_layer_up, move_layer_down]
        general_settings_frame = sgtk.Frame(self.win, num_cols=len(settings_cols), color=WIN_CONTENT_COLOR, debug_name="general_layer_settings")
        for column, widget in enumerate(settings_cols):
            general_settings_frame.add_grid_child(widget, col=column)

        # layer rows
        # TODO non-static number of rows
        layer_rows_frame = sgtk.Frame(self.win, num_cols=3, num_rows=10, color=WIDGET_BRIGHT_COLOR, pad_x=4, pad_y=4, debug_name="layer_rows")
        def reorder_layer_rows_frame():
            # detach all widgets from layer_rows_frame and store them
            rows = {} # "draw_layer_name" : [widgets, in, that, row]
            for row_i in range(layer_rows_frame.get_num_rows()):
                row = []
                for col_i in range(layer_rows_frame.get_num_cols()):
                    widget = layer_rows_frame.get_grid_child(col=col_i, row=row_i)
                    if widget is None: break
                    layer_rows_frame.remove_child(widget, update=False)
                    row.append(widget)
                if len(row) > 0: rows[row[0].get_text()] = row

            # place the rows back in reversed order in which they are
            # stored in self.draw_layer_man, so the last drawn layer is at the top
            dest_row_i = 0
            for row_i in reversed(range(self.draw_layer_man.get_num_layers())):
                draw_layer = self.draw_layer_man.get_layer_by_row(row_i)
                row = rows[draw_layer.name]
                for col_i in range(layer_rows_frame.get_num_cols()):
                    layer_rows_frame.add_grid_child(row[col_i], col=col_i, row=dest_row_i)
                dest_row_i += 1

        def on_draw_layer_add(draw_layer: DrawLayer):
            kwargs = {**button_theme, "pad_x": 4, "pad_y": 4}

            layer_button = sgtk.Button(self.win, text=draw_layer.name,
                cmd=lambda: self.draw_layer_man.set_layer(draw_layer), **{**kwargs,
                "deactive_color": TEXT_BRIGHT_COLOR, "deactive_bg_color": HIGHLIGHT_COLOR
            })
            def layer_button_on_draw_layer_name_set(data: tuple):
                layer = data[0]
                if layer is not draw_layer: return
                layer_button.set_text(layer.name)
            self.event.listen(DRAW_LAYER_NAME_SET, layer_button_on_draw_layer_name_set)
            def layer_button_close(update:bool=True):
                self.event.unlisten(DRAW_LAYER_NAME_SET, layer_button_on_draw_layer_name_set)
                sgtk.Button.close(layer_button, update=update)
            layer_button.close = layer_button_close

            def create_vis_btn_cmd() -> callable:
                def cmd(): self.toggle_draw_layer_hidden(draw_layer)
                return cmd
            vis_button = sgtk.Button(self.win, text="hide", cmd=create_vis_btn_cmd(), **kwargs)
            def vis_button_on_draw_layer_hidden(event_draw_layer: DrawLayer):
                if event_draw_layer is draw_layer:
                    vis_button.set_text("show" if draw_layer.is_hidden() else "hide")
            self.event.listen(DRAW_LAYER_HIDDEN, vis_button_on_draw_layer_hidden)
            def vis_button_close(update:bool=True):
                self.event.unlisten(DRAW_LAYER_HIDDEN, vis_button_on_draw_layer_hidden)
                sgtk.Button.close(vis_button, update=update)
            vis_button.close = vis_button_close

            settings_button = sgtk.Button(self.win, text="...", cmd=lambda: LayerSettings(draw_layer), **kwargs)
            # TODO
            #settings_button.deactivate()

            columns = [layer_button, vis_button, settings_button]
            row = layer_rows_frame.get_num_rows() - 1
            for col, widget in enumerate(columns):
                layer_rows_frame.add_grid_child(widget, col=col, row=row)

            reorder_layer_rows_frame()

        def get_frame_row_of_draw_layer(draw_layer: DrawLayer) -> int:
            for row in range(layer_rows_frame.get_num_rows()):
                layer_btn = layer_rows_frame.get_grid_child(col=0, row=row)
                if layer_btn is not None and layer_btn.get_text() == draw_layer.name: return row
            return -1

        def on_draw_layer_remove(draw_layer: DrawLayer):
            row = get_frame_row_of_draw_layer(draw_layer)
            for col in range(layer_rows_frame.get_num_cols()):
                layer_rows_frame.get_grid_child(col=col, row=row).close()
            reorder_layer_rows_frame()

        def on_draw_layer_set_event(data: tuple):
            prev_draw_layer, new_draw_layer = data
            if prev_draw_layer is not None:
                row = get_frame_row_of_draw_layer(prev_draw_layer)
                if row >= 0:
                    layer_rows_frame.get_grid_child(col=0, row=row).activate()
            if new_draw_layer is not None:
                row = get_frame_row_of_draw_layer(new_draw_layer)
                if row >= 0:
                    layer_rows_frame.get_grid_child(col=0, row=row).deactivate()

        self.event.listen(DRAW_LAYER_ADDED, on_draw_layer_add)
        self.event.listen(DRAW_LAYER_REMOVED, on_draw_layer_remove)
        self.event.listen(DRAW_LAYER_MOVED, lambda data: reorder_layer_rows_frame())
        self.event.listen(DRAW_LAYER_SET, on_draw_layer_set_event)

        layer_frame = sgtk.Frame(self.win, 1, 2, color=WIN_CONTENT_COLOR, pad_y=12, debug_name="layer_frame")
        layer_frame.add_grid_child(general_settings_frame, row=0)
        layer_frame.add_grid_child(layer_rows_frame,       row=1)
        return layer_frame

    def _move_draw_layer_selection(self, up: bool):
        current_row = self.draw_layer_man.get_layer_row(self.draw_layer_man.get_layer())
        if current_row is None: return
        row = current_row + (1 if up else -1)
        if row < 0 or row >= self.draw_layer_man.get_num_layers(): return
        self.draw_layer_man.set_layer(self.draw_layer_man.get_layer_by_row(row))

    def _move_current_draw_layer(self, up: bool):
        draw_layer = self.draw_layer_man.get_layer()
        if draw_layer is None: return
        delta_move = 1 if up else -1
        self.draw_layer_man.set_layer_row(draw_layer, self.draw_layer_man.get_layer_row(draw_layer) + delta_move)

    def _add_texture(self):
        draw_layer = self.draw_layer_man.get_layer()
        if draw_layer is None: return
        if draw_layer.src_tex_name.split(" ")[0] == "data":
            self.texture_collection.setup_layer(draw_layer.src_tex_name, draw_layer.ts, None)
        else:
            self._add_texture_btn.deactivate()
            start_dir = None if self._last_save_path is None else self._last_save_path.parent
            self._open_texture_dialog = sgtk.OpenFile(self.tk, ["*.png", "*.bmp"], title="Open texture...", start_dir=start_dir, **explorer_theme)

    def _add_layer(self):
        if 0 in (self.draw_layer_man.w, self.draw_layer_man.h): return
        NewLayer(self)

    def _remove_layer(self):
        draw_layer = self.draw_layer_man.get_layer()
        if draw_layer is None or self._ask_remove_layer_dialog is not None:
            return
        def callback():
            self._ask_remove_layer_dialog = None
        self._ask_remove_layer_dialog = AskRemoveLayer(self.tk,
            draw_layer.name, callback=callback,
            callback_on_ok=lambda: self.draw_layer_man.remove_layer(draw_layer)
        )

    def save_file_as(self):
        if self._save_dialog is not None: return # there already is a SaveFile window...
        self._save_map_btn.deactivate() # don't let the user click the "Save Map" Button until the SaveFile dialog is closed

        start_dir = None if self._last_save_path is None else self._last_save_path.parent
        self._save_dialog = sgtk.SaveFile(self.tk, "*.json", ensure_file_type=True,
            title="Save Map (*.json)", start_dir=start_dir, AlreadyExistsDialog=FileAlreadyExists,
            **explorer_theme
        )

    def _save_file(self):
        if self._last_save_path is None: return
        data = self.save()
        with self._last_save_path.open("w") as map_file:
            json.dump(data, map_file, separators=(",", ":"))
        Dirtiness.reset()

    def _ask_load_file(self):
        self._load_map_btn.deactivate()

        start_dir = None if self._last_save_path is None else self._last_save_path.parent
        self._load_dialog = sgtk.OpenFile(self.tk, ["*.json"],
            title="Load Map (*.json)", start_dir=start_dir, **explorer_theme
        )

    def _load_file(self, path: Path):
        if path is None: return
        with path.open() as map_file:
            data = json.load(map_file)
        self._last_save_path = path
        self.load(data)
        self._last_save_path = path
        Dirtiness.reset()

    def _update_title(self, *_):
        if self._last_save_path is None: name = "[Untitled Map]"
        else: name = self._last_save_path.name
        title = f"{name} - {self._title}"
        if Dirtiness.get(): title = f"* {title} *"

        if self.map_widget is not None and self.draw_layer_man is not None:
            pos = self.draw_layer_man.get_cursor_tile_pos(*self.map_widget.get_rel_mouse_pos())
            if pos is not None:
                title = f"{title} - Tile: {pos[0]}, {pos[1]}"

        self.win.set_title(title)

    def _new_map(self):
        if Dirtiness.get(): self._warn_if_unsaved(self._ask_new_map)
        else: self._ask_new_map()
    def _ask_new_map(self):
        if self._new_map_dialog is not None: return
        self._new_map_btn.deactivate()
        self._new_map_dialog = NewMap(self)

    def _on_new_map(self, _data: None):
        self._last_save_path = None
        self._update_title()

    def _on_key_press(self, sdl2_keycode: int):
        ctrl = self.tk.keyboard.get_mod("ctrl")
        shift = self.tk.keyboard.get_mod("shift")
        if ctrl:
            if   sdl2_keycode == SDLK_z: self.undo()
            elif sdl2_keycode == SDLK_y: self.redo()
            elif sdl2_keycode == SDLK_n: self._new_map()
            elif sdl2_keycode == SDLK_h: self.toggle_draw_layer_hidden(self.draw_layer_man.get_layer())
            elif sdl2_keycode == SDLK_l: self.load_file()
            elif sdl2_keycode == SDLK_s: self.save_file()
            elif sdl2_keycode == SDLK_g and shift: self.tk.create_graph()

        elif self.tk.keyboard.is_pressed(SDLK_l):
            if shift:
                if   sdl2_keycode == SDLK_UP:   self._move_current_draw_layer(True)
                elif sdl2_keycode == SDLK_DOWN: self._move_current_draw_layer(False)
            else:
                if   sdl2_keycode == SDLK_PLUS:  self._add_layer()
                elif sdl2_keycode == SDLK_MINUS: self._remove_layer()
                elif sdl2_keycode == SDLK_UP:    self._move_draw_layer_selection(True)
                elif sdl2_keycode == SDLK_DOWN:  self._move_draw_layer_selection(False)

        elif self.tk.keyboard.is_pressed(SDLK_t):
            if sdl2_keycode == SDLK_PLUS: self._add_texture()

        elif sdl2_keycode in (SDLK_w, SDLK_a, SDLK_s, SDLK_d):
            texture_layer_man = self.texture_collection.get_current_manager()
            if texture_layer_man is not None:
                tile_x, tile_y = texture_layer_man.get_selected_tile_pos()
                if   sdl2_keycode == SDLK_a: tile_x -= 1
                elif sdl2_keycode == SDLK_d: tile_x += 1
                elif sdl2_keycode == SDLK_w: tile_y -= 1
                elif sdl2_keycode == SDLK_s: tile_y += 1
                texture_layer_man.set_selected_tile(tile_x, tile_y)

        elif sdl2_keycode == SDLK_F5:
            self.event.emit(REFRESH_TEXTURES, None)
            self.event.emit(REFRESH_DRAW_LAYERS, None)

    def _warn_if_unsaved(self, callback_on_ok: callable):
        if self._warn_unsaved_dialog is not None: return
        def callback():
            self._warn_unsaved_dialog = None
        self._warn_unsaved_dialog = WarnUnsaved(self.tk, callback_on_ok=callback_on_ok, callback=callback)

    def _on_tk_close_request(self) -> bool:
        if not Dirtiness.get(): return True
        self._warn_if_unsaved(self.tk.stop)
        return False

    def _on_window_closed(self, window: sgtk.Window):
        if self._new_map_dialog is not None and window is self._new_map_dialog.get_window():
            self._new_map_dialog = None
            self._new_map_btn.activate()
