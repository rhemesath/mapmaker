#!/usr/bin/python3

import sys
import json
from pathlib import Path
# Set Python's import path to this files' directory.
# This also makes this file executable from any current working directory
this_dir = Path(__file__).parent.resolve()
sys.path.insert(0, str(this_dir))

try:
    import sgtk
except ModuleNotFoundError:
    # The symbolic link was not used, probably because we are on Windows.
    # So we have to add the PySGTK Git submodule directory for import
    sys.path.insert(1, str(Path(this_dir, "PySGTK")))

from MapMaker import MapMaker

from MapWidget import MapWidget
from MapWidgetInput import MapWidgetInput
from MapWidgetMouseInput import MapWidgetMouseInput
from MapWidgetTouchpadInput import MapWidgetTouchpadInput

def main(argv: list):
    config_file_path = this_dir.joinpath("config.json")
    if not config_file_path.exists(): config_file_path = this_dir.joinpath("config.json.sample")
    with config_file_path.open() as config_file:
        config = json.load(config_file)

    input_type = config["input"]["use"]
    MapWidgetInputType = {
        "mouse": MapWidgetMouseInput,
        "touchpad": MapWidgetTouchpadInput
    }[input_type]
    def MapWidgetInputFactory(map_widget: MapWidget) -> MapWidgetInput:
        return MapWidgetInputType(map_widget, **config["input"][input_type])

    map_maker = MapMaker(**config["sgtk"], MapWidgetInputFactory=MapWidgetInputFactory)
    map_maker.run()

if __name__ == "__main__":
    main(argv=sys.argv[1:])
