from pathlib import Path

from TextureLayer import TextureLayer
from DataLayer import DataLayer
from TextureLayerManager import TextureLayerManager
from events import TEXTURE_UPDATED
from Dirtiness import Dirtiness
from Restorable import Restorable

# Basically, this whole class is a workaround for bad architecture

class TextureLayerCollection(Restorable):
    def __init__(self, map_maker):
        self.map_maker = map_maker

        self._managers = {} # {name: {16: TextureLayerManager, 32: TextureLayerManager}, name2: ...}

    def request_new_layer(self, name: str, tile_size: int):
        managers = self._managers.get(name)
        if managers is None:
            managers = {}
            self._managers[name] = managers

        if tile_size in managers: return

        if len(managers) == 0: file_path = None
        else: file_path = next(iter(managers.values())).get_file_path()

        self.new_layer(name, tile_size, file_path)
        if file_path is not None:
            self.setup_layer(name, tile_size, file_path)

    def update_file_path(self, name: str, file_path: Path):
        managers = self._managers[name]
        for manager in managers.values():
            manager.set_file_path(file_path)
            if manager.get_layer() is None:
                manager.set_layer(TextureLayer(manager, name))

    def new_layer(self, name: str, tile_size: int, file_path: Path):
        self._managers[name][tile_size] = TextureLayerManager(self.map_maker, tile_size, file_path)
        self.map_maker.event.emit(TEXTURE_UPDATED, name)
        Dirtiness.change()

    def setup_layer(self, name: str, tile_size: int, file_path: Path):
        texture_layer_man = self.get_manager(name, tile_size)

        if name.split(" ")[0] == "data":
            layer = DataLayer(texture_layer_man, name)
            texture_layer_man.set_layer(layer)
        else:
            if file_path is None: return
            self.update_file_path(name, file_path)
        self.map_maker.event.emit(TEXTURE_UPDATED, name)
        Dirtiness.change()

    def get_layer(self, name: str, tile_size: int) -> TextureLayer:
        return self._managers[name][tile_size].get_layer()

    def get_manager(self, name: str, tile_size: int) -> TextureLayerManager:
        """ returns Optional[TextureLayerManager] """
        managers = self._managers.get(name)
        if managers is None: return None
        return managers.get(tile_size)

    def get_current_manager(self) -> TextureLayerManager:
        """ returns Optional[TextureLayerManager] """
        draw_layer = self.map_maker.draw_layer_man.get_layer()
        if draw_layer is None: return None
        return self.get_manager(draw_layer.src_tex_name, draw_layer.ts)

    def load(self, data: dict):
        texture_data = data["textures"]

        for draw_layer_data in data["layers"]:
            name = draw_layer_data["src_tex_name"]
            tile_size = draw_layer_data["tile_size"]
            path = texture_data[name]
            if path is not None: path = Path(path)

            self.request_new_layer(name, tile_size)
            self.setup_layer(name, tile_size, path)

    def save(self) -> dict:
        data = {}
        for name in self._managers:
            manager = list(self._managers[name].values())[0]
            file_path = manager.get_file_path()
            if file_path is not None: file_path = str(file_path.resolve())
            data[name] = file_path

        return data
