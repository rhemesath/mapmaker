from ctypes import POINTER
from sdl2 import SDL_DestroyTexture, SDL_Texture

from Layer import Layer
from theme import WIDGET_BRIGHT_COLOR, WIDGET_BRIGHTER_COLOR

class BackgroundLayer(Layer):
    def __init__(self, layer_manager):
        super().__init__(layer_manager, 4)

        self.tile_w = self.layer_man.w // self.ts
        self.tile_h = self.layer_man.h // self.ts

        self._src_tex: POINTER(SDL_Texture) = None

        self._setup()

    def _setup(self):
        self._setup_texture()

    def _setup_texture(self):
        # create source texture (2x2 checkerboard pattern)
        self._src_tex = self.paint.create_target_texture(self.ts, self.ts)
        self.paint.set_target_texture(self._src_tex)
        size = self.ts // 2
        for y, line in enumerate([
            (WIDGET_BRIGHT_COLOR, WIDGET_BRIGHTER_COLOR),
            (WIDGET_BRIGHTER_COLOR, WIDGET_BRIGHT_COLOR)
        ]):
            for x, color in enumerate(line):
                self.paint.rect(color, x*size, y*size, size, size)

        # create own texture
        self._tex = self.paint.create_target_texture(self.layer_man.w, self.layer_man.h)
        with self.paint.target_texture(self._tex):

            for tile_y in range(self.tile_h):
                y = tile_y * self.ts
                for tile_x in range(self.tile_w):
                    self.paint.texture(self._src_tex, tile_x * self.ts, y, self.ts, self.ts)

    def _setup_tile_map(self): pass

    def full_draw(self): pass

    def close(self):
        super().close()
        SDL_DestroyTexture(self._src_tex)
