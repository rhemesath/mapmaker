from ctypes import POINTER
from sdl2 import SDL_Texture, SDL_DestroyTexture

class Layer:
    def __init__(self, layer_manager, tile_size: int):
        self.layer_man = layer_manager
        self.map_maker = layer_manager.map_maker
        self.ts = tile_size
        self.tile_map: list = []

        self.paint = self.layer_man.paint

        self.tile_w: int = 0
        self.tile_h: int = 0

        self._tex: POINTER(SDL_Texture) = None

    def _setup(self):
        self._setup_tile_map()
        self._setup_texture()
        self.layer_man.tell_size(self.tile_w * self.ts, self.tile_h * self.ts)

    def _setup_tile_map(self):
        self.tile_w = len(self.tile_map[0])
        self.tile_h = len(self.tile_map)

    def _setup_texture(self):
        raise NotImplementedError()

    def get_tile_rect_from_texel(self, texel_x: int, texel_y: int) -> tuple:
        """ Returns None if the tile doesn't exist """
        tile_x = texel_x // self.ts
        tile_y = texel_y // self.ts
        if not self.tile_exists(tile_x, tile_y): return None
        return (tile_x * self.ts, tile_y * self.ts, self.ts, self.ts)

    def texel_x_to_tile_x(self, texel_x: float) -> int:
        return int(texel_x) // self.ts
    def texel_y_to_tile_y(self, texel_y: float) -> int:
        return int(texel_y) // self.ts

    def tile_exists(self, tile_x: int, tile_y: int):
        return (
            tile_x >= 0 and tile_x < self.tile_w and
            tile_y >= 0 and tile_y < self.tile_h
        )

    def close(self):
        SDL_DestroyTexture(self._tex)

    def draw_tile_index(self, tile_i: int, dest_tile_x: int, dest_tile_y: int):
        self.paint.texture(self._tex,
            dest_tile_x * self.ts, dest_tile_y * self.ts, self.ts, self.ts,
            src=(
                tile_i % self.tile_w * self.ts,
                tile_i // self.tile_w * self.ts,
                self.ts, self.ts
            )
        )

    def _draw_tile(self, tile_x: int, tile_y: int):
        pass

    def set_tile(self, tile: int, tile_x: int, tile_y: int):
        if not self.tile_exists(tile_x, tile_y): return
        self.tile_map[tile_y][tile_x] = tile
        self._draw_tile(tile_x, tile_y)

    def get_tile(self, tile_x: int, tile_y: int) -> int:
        if not self.tile_exists(tile_x, tile_y): return -1
        return self.tile_map[tile_y][tile_x]

    def full_draw(self):
        pass

    def render(self, ren_x: int, ren_y: int, ren_w: int, ren_h: int):
        self.paint.texture(self._tex, ren_x, ren_y, ren_w, ren_h)
