from abc import ABC, abstractmethod

class Savable(ABC):
    @abstractmethod
    def save(self) -> dict:
        pass

class Loadable(ABC):
    @abstractmethod
    def load(self, data: dict):
        pass

class Restorable(Savable, Loadable):
    pass
