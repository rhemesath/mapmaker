
# Based on the Arc-Dark theme
WIN_BASE_COLOR        = (47, 52, 63)
WIN_BASE_DARKER_COLOR = (36, 38, 46)
WIN_CONTENT_COLOR     = (53, 57, 69)
WIDGET_COLOR          = (64, 69, 82)
WIDGET_BRIGHTER_COLOR = (103, 107, 120)
WIDGET_BRIGHT_COLOR   = (98, 102, 114)
HIGHLIGHT_COLOR       = (82, 148, 226)
FORBIDDEN_COLOR       = (204, 87, 93) #(252, 65, 56)
TEXT_BRIGHT_COLOR     = (255, 255, 255)
TEXT_COLOR            = (220, 220, 220)
TEXT_DARK_COLOR       = (200, 200, 200)
TEXT_DARKER_COLOR     = (130, 130, 130)
TEXT_SELECTION_COLOR  = (*HIGHLIGHT_COLOR, 100)

button_theme = {
    "pad_x": 2, "ipad_x": 7, "color": TEXT_BRIGHT_COLOR, "bg_color": WIDGET_COLOR,
    "hl_bg_color": HIGHLIGHT_COLOR, "deactive_color": TEXT_DARKER_COLOR,
    "deactive_bg_color": WIDGET_COLOR
}

explorer_theme = {
    "window_theme": {
        "color": WIN_BASE_COLOR
    },

    "nav_bar_theme": {
        "color": WIN_BASE_COLOR
    },
    "nav_bar_btn_theme": {
        "bg_color": WIN_BASE_COLOR,
        "hl_bg_color": HIGHLIGHT_COLOR,
        "font_size": 14,
        "pad_x": 4,
        "pad_y": 4,
        "ipad_x": 8
    },
    "nav_bar_path_theme": {
        "color": TEXT_COLOR,
        "bg_color": WIDGET_COLOR,
        "active_bg_color": WIDGET_COLOR,
        "highlight_color": TEXT_SELECTION_COLOR,
        "min_w": 300,
        "font_size": 14,
        "pad_x": 4,
        "pad_y": 4
    },

    "explorer_theme": {
        "color": WIN_CONTENT_COLOR,
        "pad_x": 2,
        "pad_y": 2
    },
    "explorer_btn_theme": {
        "bg_color": WIN_CONTENT_COLOR,
        "hl_bg_color": WIDGET_COLOR,
        "deactive_color": TEXT_DARK_COLOR,
        "deactive_bg_color": FORBIDDEN_COLOR,
        "pad_x": 2,
        "pad_y": 2
    },

    "action_bar_theme": {
        "color": WIN_BASE_COLOR
    },
    "action_bar_btn_theme": {
        "bg_color": WIDGET_COLOR,
        "hl_bg_color": HIGHLIGHT_COLOR,
        "deactive_color": TEXT_COLOR,
        "deactive_bg_color": FORBIDDEN_COLOR,
        "font_size": 14,
        "pad_x": 4,
        "pad_y": 4,
        "ipad_x": 4
    },

    "selected_bg_color": HIGHLIGHT_COLOR
}

_main_text_theme = {
    "color": TEXT_COLOR, "bg_color": WIN_CONTENT_COLOR, "font_size": 14,
    "pad_x": 4, "pad_y": 4
}
dialog_theme = {
    "window": {"color": WIN_BASE_COLOR},

    "main_frame": {"color": WIN_CONTENT_COLOR},
    "main_text" : _main_text_theme,
    "main_input": {**_main_text_theme,
        "bg_color": WIDGET_COLOR, "min_w": 250, "highlight_color": TEXT_SELECTION_COLOR,
        "active_bg_color": WIDGET_COLOR
    },
    "main_btn": {**_main_text_theme,
        "hl_bg_color": HIGHLIGHT_COLOR,
        "deactive_color": TEXT_COLOR, "deactive_bg_color": HIGHLIGHT_COLOR,
        "ipad_x": 4, "ipad_y": 4
    },

    "action_frame" : {"color": WIN_BASE_COLOR},
    "action_btn" : {
        "bg_color": WIDGET_COLOR, "hl_bg_color": HIGHLIGHT_COLOR,
        "deactive_color": TEXT_COLOR, "deactive_bg_color": FORBIDDEN_COLOR,
        "font_size": 14,
        "pad_x": 4, "pad_y": 4, "ipad_x": 4
    }
}

yes_or_cancel_dialog_theme = {
    "window_theme": dialog_theme["window"],
    "text_theme":   dialog_theme["main_text"],
    "action_bar_theme":     dialog_theme["action_frame"],
    "action_bar_btn_theme": dialog_theme["action_btn"]
}
