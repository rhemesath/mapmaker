from sdl2 import SDL_SetTextureBlendMode, SDL_BLENDMODE_BLEND

from Layer import Layer
from TextureLayer import TextureLayer
from CommandManager import CommandManager
from Restorable import Savable
from Dirtiness import Dirtiness
from events import (
    TEXTURE_UPDATED, DRAW_LAYER_SET, DRAW_LAYER_HIDDEN, REFRESH_DRAW_LAYERS,
    DRAW_LAYER_NAME_SET
)

class DrawLayer(Layer, Savable):
    def __init__(self, layer_manager, tile_size: int, src_texture_name: str,
        tile_map:list=None, name:str=None
    ):
        super().__init__(layer_manager, tile_size)

        self.cmd = CommandManager(self.map_maker.event)
        self.src_tex_name = src_texture_name
        self.name = name or self.layer_man.create_layer_name()
        self._hidden = False
        self._save_positions = False

        self.tile_map: list
        if tile_map is not None: self.tile_map = tile_map
        else:
            self.tile_map = [[0 for _ in range(self.layer_man.w // self.ts)] for _ in range(self.layer_man.h // self.ts)]

        self.map_maker.texture_collection.request_new_layer(self.src_tex_name, self.ts)

        self._setup()

        self.full_draw()
        self.map_maker.event.listen(TEXTURE_UPDATED, self._on_texture_updated)
        self.map_maker.event.listen(DRAW_LAYER_SET, self._on_draw_layer_set)
        self.map_maker.event.listen(REFRESH_DRAW_LAYERS, self._on_refresh_draw_layers)

    def change_name(self, name: str):
        name = name.strip()
        if name == self.name: return
        prev_name = self.name
        self.name = name
        Dirtiness.change()
        self.map_maker.event.emit(DRAW_LAYER_NAME_SET, (self, prev_name))

    def _draw_tile(self, tile_x: int, tile_y: int):
        tex_layer = self.get_texture_layer()
        if tex_layer is None: return

        tex_layer.set_blended(False)
        with self.paint.target_texture(self._tex):
            tex_layer.draw_tile_index(self.tile_map[tile_y][tile_x], tile_x, tile_y)
        tex_layer.set_blended(True)

    def full_draw(self):
        tex_layer = self.get_texture_layer()
        if tex_layer is None: return # TODO clear self._tex

        tex_layer.set_blended(False)
        with self.paint.target_texture(self._tex):
            for tile_y, line in enumerate(self.tile_map):
                for tile_x, tile_i in enumerate(line):
                    tex_layer.draw_tile_index(tile_i, tile_x, tile_y)
        tex_layer.set_blended(True)

    def get_texture_layer(self) -> TextureLayer:
        return self.map_maker.texture_collection.get_layer(self.src_tex_name, self.ts)

    def is_hidden(self) -> bool: return self._hidden
    def set_hidden(self, hidden: bool):
        self._hidden = hidden
        self.map_maker.event.emit(DRAW_LAYER_HIDDEN, self)

    def _setup_texture(self):
        self._tex = self.paint.create_target_texture(self.tile_w * self.ts, self.tile_h * self.ts)
        # clear texture
        with self.paint.target_texture(self._tex):
            self.paint.clear((0, 0, 0, 0))
        # enable per-pixel alpha blending for the texture
        SDL_SetTextureBlendMode(self._tex, SDL_BLENDMODE_BLEND)

    def _on_texture_updated(self, name: str):
        if name != self.src_tex_name: return
        texture_layer = self.get_texture_layer()
        if texture_layer is None: return
        self.full_draw()

    def _on_draw_layer_set(self, data: tuple):
        new_draw_layer = data[1]
        if new_draw_layer is self:
            self.cmd.emit_update()

    def _on_refresh_draw_layers(self, _):
        self.full_draw()

    def close(self):
        self.cmd.reset()
        self.map_maker.event.unlisten(TEXTURE_UPDATED, self._on_texture_updated)
        self.map_maker.event.unlisten(DRAW_LAYER_SET, self._on_draw_layer_set)
        self.map_maker.event.unlisten(REFRESH_DRAW_LAYERS, self._on_refresh_draw_layers)
        super().close()

    def set_save_positions(self, flag: bool):
        if flag != self._save_positions: Dirtiness.change()
        self._save_positions = flag
    def does_save_positions(self) -> bool:
        return self._save_positions

    @staticmethod
    def tile_map_to_positions(tile_map: list, tile_size: int) -> list:
        positions = []
        for tile_y, row in enumerate(tile_map):
            y = tile_y * tile_size
            for tile_x, tile in enumerate(row):
                if tile == 0: continue
                positions.append([tile, tile_x * tile_size, y])
        return positions

    @staticmethod
    def positions_to_tile_map(positions: list, tile_size: int, map_w: int, map_h: int) -> list:
        tile_w = map_w // tile_size
        tile_h = map_h // tile_size
        tile_map = [[0 for _ in range(tile_w)] for _ in range(tile_h)]
        for tile, x, y in positions:
            tile_map[y // tile_size][x // tile_size] = tile
        return tile_map

    def save(self) -> dict:
        tiles = self.tile_map
        if self._save_positions:
            tiles = self.tile_map_to_positions(self.tile_map, self.ts)
        return {
            "name": self.name,
            "src_tex_name": self.src_tex_name,
            "tile_size": self.ts,
            "positions": self._save_positions,
            "tiles": tiles
        }
