SGTK Tree
=========

```mermaid
graph LR;
1["SGTK"] --> 2["win <Window>"];
2["win <Window>"] --> 8["menu_bar <Frame>"];
8["menu_bar <Frame>"] --> 3["New Map <Button>"];
8["menu_bar <Frame>"] --> 5["Load Map <Button>"];
8["menu_bar <Frame>"] --> 4["Save Map <Button>"];
8["menu_bar <Frame>"] --> 6["Undo <Button>"];
8["menu_bar <Frame>"] --> 7["Redo <Button>"];
2["win <Window>"] --> 26["win_frame <Frame>"];
26["win_frame <Frame>"] --> 10["main_frame <Frame>"];
10["main_frame <Frame>"] --> 9["map_widget <MapWidget>"];
26["win_frame <Frame>"] --> 25["side_frame <Frame>"];
25["side_frame <Frame>"] --> 15["texture_frame <Frame>"];
15["texture_frame <Frame>"] --> 13["texture_menu_bar <Frame>"];
13["texture_menu_bar <Frame>"] --> 11["data <Label>"];
13["texture_menu_bar <Frame>"] --> 12["+ <Button>"];
15["texture_frame <Frame>"] --> 14["texture_map_widget <MapWidget>"];
25["side_frame <Frame>"] --> 23["layer_frame <Frame>"];
23["layer_frame <Frame>"] --> 21["general_layer_settings <Frame>"];
21["general_layer_settings <Frame>"] --> 16["Layers <Label>"];
21["general_layer_settings <Frame>"] --> 17["+ <Button>"];
21["general_layer_settings <Frame>"] --> 18["- <Button>"];
21["general_layer_settings <Frame>"] --> 19["up <Button>"];
21["general_layer_settings <Frame>"] --> 20["down <Button>"];
23["layer_frame <Frame>"] --> 22["layer_rows <Frame>"];
22["layer_rows <Frame>"] --> 68["Entities <Button>"];
22["layer_rows <Frame>"] --> 69["hide <Button>"];
22["layer_rows <Frame>"] --> 70["... <Button>"];
22["layer_rows <Frame>"] --> 65["Data <Button>"];
22["layer_rows <Frame>"] --> 66["hide <Button>"];
22["layer_rows <Frame>"] --> 67["... <Button>"];
22["layer_rows <Frame>"] --> 62["Floor <Button>"];
22["layer_rows <Frame>"] --> 63["hide <Button>"];
22["layer_rows <Frame>"] --> 64["... <Button>"];
25["side_frame <Frame>"] --> 24["TEST <Label>"];
```